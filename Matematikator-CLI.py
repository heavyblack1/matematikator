#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import math
from time import sleep
import os
import yaml
__author__ = "heavyblack"
__version__ = "0.9"
__license__ = "GPLv3"
__email__ = "heavyblack@gmail.com"


def logo():
    print("""
        ███╗   ███╗ █████╗ ████████╗███████╗███╗   ███╗ █████╗ ████████╗██╗██╗  ██╗ █████╗ ████████╗ ██████╗ ██████╗
        ████╗ ████║██╔══██╗╚══██╔══╝██╔════╝████╗ ████║██╔══██╗╚══██╔══╝██║██║ ██╔╝██╔══██╗╚══██╔══╝██╔═══██╗██╔══██╗
        ██╔████╔██║███████║   ██║   █████╗  ██╔████╔██║███████║   ██║   ██║█████╔╝ ███████║   ██║   ██║   ██║██████╔╝
        ██║╚██╔╝██║██╔══██║   ██║   ██╔══╝  ██║╚██╔╝██║██╔══██║   ██║   ██║██╔═██╗ ██╔══██║   ██║   ██║   ██║██╔══██╗
        ██║ ╚═╝ ██║██║  ██║   ██║   ███████╗██║ ╚═╝ ██║██║  ██║   ██║   ██║██║  ██╗██║  ██║   ██║   ╚██████╔╝██║  ██║
        ╚═╝     ╚═╝╚═╝  ╚═╝   ╚═╝   ╚══════╝╚═╝     ╚═╝╚═╝  ╚═╝   ╚═╝   ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝



    """)


# maximum vypočetních operací které lze zadat
max_volba = 9
# maximum Number-theoretic and representation
max_volba_num = 104
# maximum Trigonometric functions
max_volba_tri = 204


def nacti_cislo(cele_cislo=False, text=""):
    """
    nacte cislo
    """
    spatne = True
    while spatne:
        try:
            if cele_cislo:
                cislo = int(input(text))
                spatne = False
            else:
                # input vraci str float() prevede do float a odstrani se pomocí cislo.replace() spatna desetina carka
                cislo = input(text)
                cislo = float(cislo.replace(",","."))
                spatne = False
            return cislo
        except ValueError:
            print("špatné číslo")


def load_yn(text):
    """
     load yes or no
    """

    nezadano = True
    while nezadano:
        odpoved = input(text)
        if ((odpoved == "y") or (odpoved == "Y")):
            return True

        if ((odpoved == "n") or (odpoved == "N")):
            return False


def yaml_loader(yaml_to_load):
    """
    nacita yaml soubory
    save yaml file
    """
    try:
        with open(yaml_to_load, "r", encoding="utf-8") as yaml_conf:
            yaml_file = yaml.safe_load(yaml_conf)
            yaml_conf.close
            return yaml_file
    except OSError:
        print("Can't load setting")
        setti = {"settings": {"first_run": "error"}}
        return setti


def yaml_saver(yaml_to_save, data_to_save):
    """
    uklada data do yaml
    load yaml file
    """
    try:
        with open(yaml_to_save, "w", encoding="utf-8") as yaml_conf:
            yaml.safe_dump(data_to_save, yaml_conf)
    except OSError:
        print("Can't safe setting")


def printlog(vys_h, vysledek, cislo1, cislo2):
    try:
        with open("printlog", "a", encoding="utf-8") as pl:
            pl.write("\n###############################\n{c1}\n{c2}\n{h}\n{v}".format(
                h=vys_h, v=vysledek, c1=cislo1, c2=cislo2))
    except OSError:
        print("Can't safe printlog")


def volba():
    spatne = True
    while spatne:

        # source
        # https://docs.python.org/3/library/math.html
        # https://www.itnetwork.cz/python/zaklady/python-tutorial-knihovny-math-a-random/
        print(
            """
###############################################################
{s_help}
###############################################################
1 - {addition}
2 - {subtraction}
3 - {multiplication}
4 - {division}
###############################################################
{num_th}
100 - ceil(x)
# Return the ceiling of x, the smallest integer greater than or equal to x.
101 - copysign(x, y)
# Return a float with the magnitude (absolute value) of x but the sign of y.
102 - fabs(x)
# Return the absolute value of x.
103 - factorial(x)
# Return x factorial. Raises ValueError if x is not integral or is negative.
104 - floor(x)
# Return the floor of x, the largest integer less than or equal to x.
# If x is not a float, delegates to x.__floor__(), which should return an Integral value.
105 - fmod(x, y)
Return fmod(x, y), as defined by the platform C library. Note that the Python expression x % y may not return the same result. The intent of the C standard is that fmod(x, y) be exactly (mathematically; to infinite precision) equal to x - n*y for some integer n such that the result has the same sign as x and magnitude less than abs(y). Python’s x % y returns a result with the sign of y instead, and may not be exactly computable for float arguments. For example, fmod(-1e-100, 1e100) is -1e-100, but the result of Python’s -1e-100 % 1e100 is 1e100-1e-100, which cannot be represented exactly as a float, and rounds to the surprising 1e100. For this reason, function fmod() is generally preferred when working with floats, while Python’s x % y is preferred when working with integers.
106 - frexp(x)
Return the mantissa and exponent of x as the pair (m, e). m is a float and e is an integer such that x == m * 2**e exactly. If x is zero, returns (0.0, 0), otherwise 0.5 <= abs(m) < 1. This is used to “pick apart” the internal representation of a float in a portable way.
107 - fsum(x)
write number in format 4,4,5,6 float number as 4.5 not 4,5
108 - gcd(a, b)
Return the greatest common divisor of the integers a and b. If either a or b is nonzero, then the value of gcd(a, b) is the largest positive integer that divides both a and b. gcd(0, 0) returns 0.
109 - isclose(a, b, *, rel_tol=1e-09, abs_tol=0.0)
Return True if the values a and b are close to each other and False otherwise.
Whether or not two values are considered close is determined according to given absolute and relative tolerances.
rel_tol is the relative tolerance – it is the maximum allowed difference between a and b, relative to the larger absolute value of a or b. For example, to set a tolerance of 5%, pass rel_tol=0.05. The default tolerance is 1e-09, which assures that the two values are the same within about 9 decimal digits. rel_tol must be greater than zero.
abs_tol is the minimum absolute tolerance – useful for comparisons near zero. abs_tol must be at least zero.
If no errors occur, the result will be: abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol).
The IEEE 754 special values of NaN, inf, and -inf will be handled according to IEEE rules. Specifically, NaN is not considered close to any other value, including NaN. inf and -inf are only considered close to themselves.
110 - isfinite(x)
Return True if x is neither an infinity nor a NaN, and False otherwise. (Note that 0.0 is considered finite.)
111 - isinf(x)
Return True if x is a positive or negative infinity, and False otherwise.
112 - isnan(x)
Return True if x is a NaN (not a number), and False otherwise.
113 - ldexp(x, i)
Return x * (2**i). This is essentially the inverse of function frexp().
114 - modf(x)
Return the fractional and integer parts of x. Both results carry the sign of x and are floats.
115 - remainder(x, y)
Return the IEEE 754-style remainder of x with respect to y. For finite x and finite nonzero y, this is the difference x - n*y, where n is the closest integer to the exact value of the quotient x / y. If x / y is exactly halfway between two consecutive integers, the nearest even integer is used for n. The remainder r = remainder(x, y) thus always satisfies abs(r) <= 0.5 * abs(y).
Special cases follow IEEE 754: in particular, remainder(x, math.inf) is x for any finite x, and remainder(x, 0) and remainder(math.inf, x) raise ValueError for any non-NaN x. If the result of the remainder operation is zero, that zero will have the same sign as x.
On platforms using IEEE 754 binary floating-point, the result of this operation is always exactly representable: no rounding error is introduced.
116 - trunc(x)
Return the Real value x truncated to an Integral (usually an integer). Delegates to x.__trunc__().
Note that frexp() and modf() have a different call/return pattern than their C equivalents: they take a single argument and return a pair of values, rather than returning their second return value through an ‘output parameter’ (there is no such thing in Python).
For the ceil(), floor(), and modf() functions, note that all floating-point numbers of sufficiently large magnitude are exact integers. Python floats typically carry no more than 53 bits of precision (the same as the platform C double type), in which case any float x with abs(x) >= 2**52 necessarily has no fractional bits.
###############################################################
{p_log}
5 - {exponentiation}
6 - {rooting}
10 - exp(x)
Return e raised to the power x, where e = 2.718281… is the base of natural logarithms. This is usually more accurate than math.e ** x or pow(math.e, x).
11 - expm1(x)
12 - log(x[, base])
With one argument, return the natural logarithm of x (to base e).
With two arguments, return the logarithm of x to the given base, calculated as log(x)/log(base).
13 - log1p(x)
Return the natural logarithm of 1+x (base e). The result is calculated in a way which is accurate for x near zero.
14 - log2(x)
Return the base-2 logarithm of x. This is usually more accurate than log(x, 2).
15 - log10(x)
Return the base-10 logarithm of x. This is usually more accurate than log(x, 10).
16 -  pow(x, y)
Return x raised to the power y. Exceptional cases follow Annex ‘F’ of the C99 standard as far as possible. In particular, pow(1.0, x) and pow(x, 0.0) always return 1.0, even when x is a zero or a NaN. If both x and y are finite, x is negative, and y is not an integer then pow(x, y) is undefined, and raises ValueError.
Unlike the built-in ** operator, math.pow() converts both its arguments to type float. Use ** or the built-in pow() function for computing exact integer powers.
###############################################################
{trigo_f}
7 - {sine}
8 - {cosine}
9 - {tan}
200 - acos(x)
# Return the arc cosine of x.
201 - asin(x)
# Return the arc sine of x.
202 - atan(x)
# Return the arc tangent of x.
203 - atan2(y, x)
# Return atan(y / x), in radians. The result is between -pi and pi. The vector in the plane from the origin to point (x, y) makes this angle with the positive X axis. The point of atan2() is that the signs of both inputs are known to it, so it can compute the correct quadrant for the angle. For example, atan(1) and atan2(1, 1) are both pi/4, but atan2(-1, -1) is -3*pi/4.
204 - hypot(x, y)
# Return the Euclidean norm, sqrt(x*x + y*y). This is the length of the vector from the origin to point (x, y).
{angular}
205 - degrees(x)
Convert angle x from radians to degrees.
206 - radians(x)
Convert angle x from degrees to radians.
{hyp_f}
207 - acosh(x)
Return the inverse hyperbolic cosine of x.
208 - asinh(x)
Return the inverse hyperbolic sine of x.
209 - atanh(x)
Return the inverse hyperbolic tangent of x.
210 - cosh(x)
Return the hyperbolic cosine of x.
211 - sinh(x)
Return the hyperbolic sine of x.
213 - tanh(x)
Return the hyperbolic tangent of x.
{spec_f}
214 - erf(x)
Return the error function at x.
The erf() function can be used to compute traditional statistical functions such as the cumulative standard normal distribution:
erfc(x)
Return the complementary error function at x. The complementary error function is defined as 1.0 - erf(x). It is used for large values of x where a subtraction from one would cause a loss of significance.
215 - gamma(x)
Return the Gamma function at x.
216 - lgamma(x)
Return the natural logarithm of the absolute value of the Gamma function at x.
        """.format(author=author,
                   licence_agree=licence_agree,
                   first_number=first_number,
                   second_number=second_number,
                   result=result,
                   s_help=s_help,
                   num_th=num_th,
                   p_log=p_log,
                   trigo_f=trigo_f,
                   angular=angular,
                   hyp_f=hyp_f,
                   spec_f=spec_f,
                   addition=addition,
                   subtraction=subtraction,
                   multiplication=multiplication,
                   division=division,
                   exponentiation=exponentiation,
                   rooting=rooting,
                   sine=sine,
                   cosine=cosine,
                   tan=tan)
        )
        cislo = nacti_cislo(True, "Zadajte čislo operace:")

        # rozmezí volby operace
        if ((cislo >= 1) and (cislo <= max_volba) or (cislo >= 100) and (cislo <= max_volba_num) or (cislo >= 200) and (cislo <= max_volba_tri)):
            spatne = False
            return cislo
        else:
            print("Taková volba neexistuje")
            spatne = True

    else:
        pass


def mathfunc(cislo_volby, cislo1, cislo2):
    if (cislo_volby == 1):
        vys = cislo1+cislo2
        return vys
    elif (cislo_volby == 2):
        vys = (cislo1 - cislo2)
        return vys
    elif (cislo_volby == 3):
        vys = cislo1 * cislo2
        return vys
    elif (cislo_volby == 4):
        mezivysledek = 0
        try:
            mezivysledek = cislo1 / cislo2
            vys = mezivysledek
            return vys
        except ZeroDivisionError:
            return zero_division_error
    elif (cislo_volby == 5):
        vys = cislo1 ** cislo2
        return vys
    elif (cislo_volby == 6):
        try:
            vys = math.sqrt(cislo1)
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 7):
        try:
            vys = math.sin(math.radians(cislo1))
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 8):
        try:
            vys = math.cos(math.radians(cislo1))
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 9):
        try:
            vys = math.tan(math.radians(cislo1))
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 100):
        try:
            vys = math.ceil(cislo1)
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 101):
        try:
            vys = math.copysign(cislo1, cislo2)
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 102):
        try:
            vys = math.fabs(cislo1)
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 103):
        try:
            vys = math.factorial(cislo1)
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 104):
        try:
            vys = math.floor(cislo1)
            return vys
        except ValueError:
            vys = "ValueError"
            return vys

    # math.fsum x je pole ktere je sečteno input cisla odelene carkami
    elif (cislo_volby == 200):
        try:
            vys = math.acos(math.radians(cislo1))
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 201):
        try:
            vys = math.asin(math.radians(cislo1))
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 202):
        try:
            vys = math.atan(math.radians(cislo1))
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 203):
        try:
            vys = math.atan2(cislo1, cislo2)
            return vys
        except ValueError:
            vys = "ValueError"
            return vys
    elif (cislo_volby == 204):
        try:
            vys = math.hypot(cislo1, cislo2)
            return vys
        except ValueError:
            print("ValueError")
            return "ValueError"
    else:
        vys = "Neplatná volba"
        return vys


def vysledek_hlaska(cislo_volby):
    """
    RETURN mesage because printlog need and mesage before mathfunc return result
    """
    if cislo_volby == 1:
        print(result + addition)
        return result + addition
    elif cislo_volby == 2:
        print("Výsledek odčítání")
        return "Výsledek odčítání"
    elif cislo_volby == 3:
        print("Výsledek násobení")
        return "Výsledek násobení"
    elif cislo_volby == 4:
        print("Výsledek dělení")
        return "Výsledek dělení"
    elif cislo_volby == 5:
        print("Výsledek umocňování")
        return "Výsledek umocňování"
    elif cislo_volby == 6:
        print("Výsledek odmocňování")
        return "Výsledek odmocňování"
    elif cislo_volby == 7:
        print("Výsledek Sinusu")
        return "Výsledek Sinusu"
    elif cislo_volby == 8:
        print("Výsledek Kosinusu")
        return "Výsledek Kosinusu"
    elif cislo_volby == 9:
        print("Výsledek Tangensu")
        return "Výsledek Tangensu"
    elif cislo_volby == 100:
        print("Number-theoretic and representation")
        return "Výsledek dělení"
    elif cislo_volby == 101:
        print("Výsledek dělení")
        return "Výsledek dělení"
    elif cislo_volby == 102:
        print("Výsledek dělení")
        return "Výsledek dělení"
    elif cislo_volby == 103:
        print("Výsledek dělení")
        return "Výsledek dělení"
    elif cislo_volby == 104:
        print("Výsledek dělení")
        return "Výsledek dělení"
    elif cislo_volby == 200:
        print("Výsledek dělení")
        return "Výsledek dělení"
    elif cislo_volby == 201:
        print("Výsledek dělení")
        return "Výsledek dělení"
    elif cislo_volby == 202:
        print("Výsledek dělení")
        return "Výsledek dělení"
    elif cislo_volby == 203:
        print("Výsledek dělení")
        return "Výsledek dělení"
    elif cislo_volby == 204:
        pass
    else:
        pass


def dalsi_priklad():
    """
     cyklus nezadano se opakuje dokud neni zadano spravne pismeno
     pokud je zadano y tak vrati True
     tak if dalsi priklad je True a vykona obsah podmiky protoze promena pokracovat je True cyklus pokracovat se opakuje
     pokud je zadano n tak vrati False
     tak a vykona se else obsahuje pokracovat = False a cyklus se ukonci
    """
    nezadano = True
    while nezadano:
        odpoved = input("Přejetesi vypočítat další přiklad y/n:")
        if ((odpoved == "y") or (odpoved == "Y")):
            return True
        elif ((odpoved == "n") or (odpoved == "N")):
            return False
        else:
            pass


def clear():
    os.system('cls' if os.name == 'nt' else 'clear')


def main():
    """
    setting ask
    """
    clear()
    global setti
    setti = yaml_loader(".settings.yaml")
    print(setti)

    if setti["settings"]["first_run"] == True:
        print("""
############################
1 - cz
2 - en
############################s
        """)
        again = True
        while again:
            lang_num = nacti_cislo(True, "select language:")
            if lang_num == 1:
                setti["settings"]["language"] = "cz"
                again = False
            elif lang_num == 2:
                setti["settings"]["language"] = "en"
                again = False
            else:
                pass
        lang_loader()

        clear()
        print("GNU GENERAL PUBLIC LICENSE")
        licc = load_yn(licence_agree)
        if licc == True:
            pass
        elif licc == False:
            os._exit(0)

        setti["settings"]["first_run"] = False
        os.remove(".settings.yaml")
        yaml_saver(".settings.yaml", setti)
    elif setti["settings"]["first_run"] == "error":
        print("ERROR CAN\'t load .settings.yaml\nPlease reinstall program")
        sleep(5)
        os._exit(1)
    else:
        pass

    lang_loader()

    menu()


def lang_loader():
    """
    lang loader
    """

    global author
    global licence_agree
    global first_number
    global second_number
    global is_not
    global result
    global s_help
    global num_th
    global p_log
    global trigo_f
    global angular
    global hyp_f
    global spec_f
    global addition
    global subtraction
    global multiplication
    global division
    global exponentiation
    global rooting
    global sine
    global cosine
    global tan
    global zero_division_error
    # loading mesages cz or en or other
    if setti["settings"]["language"] == "cz":
        lang = setti["language"]["cz"]
        author = lang["author"]
        licence_agree = lang["licence_agree"]
        first_number = lang["first_number"]
        second_number = lang["second_number"]
        is_not = lang["is_not"]
        result = lang["result"]
        s_help = lang["s_help"]
        num_th = lang["num_th"]
        p_log = lang["p_log"]
        trigo_f = lang["trigo_f"]
        angular = lang["angular"]
        hyp_f = lang["hyp_f"]
        spec_f = lang["spec_f"]
        addition = lang["addition"]
        subtraction = lang["subtraction"]
        multiplication = lang["multiplication"]
        division = lang["division"]
        exponentiation = lang["exponentiation"]
        rooting = lang["rooting"]
        sine = lang["sine"]
        cosine = lang["cosine"]
        tan = lang["tan"]
        zero_division_error = lang["zero_division_error"]
    elif setti["settings"]["language"] == "en":
        lang = setti["language"]["en"]
        author = lang["author"]
        licence_agree = lang["licence_agree"]
        first_number = lang["first_number"]
        second_number = lang["second_number"]
        is_not = lang["is_not"]
        result = lang["result"]
        s_help = lang["s_help"]
        num_th = lang["num_th"]
        p_log = lang["p_log"]
        trigo_f = lang["trigo_f"]
        angular = lang["angular"]
        hyp_f = lang["hyp_f"]
        spec_f = lang["spec_f"]
        addition = lang["addition"]
        subtraction = lang["subtraction"]
        multiplication = lang["multiplication"]
        division = lang["division"]
        exponentiation = lang["exponentiation"]
        rooting = lang["rooting"]
        sine = lang["sine"]
        cosine = lang["cosine"]
        tan = lang["tan"]
        zero_division_error = lang["zero_division_error"]
    else:
        print("lang error")


def menu():
    """
    Hlavni funkce vykresluje ui2
    A volají se z ní všechny funkce
    """
    pokracovat = True
    while pokracovat:
        clear()
        logo()
        print(setti["language"]["cz"]["author"] + __author__)
        cislo_volby = volba()
        # kdyz uzivatel chce mat operaci ktera bere pouze 1 cislo
        if cislo_volby == 6 or cislo_volby == 7 or cislo_volby == 8 or cislo_volby == 9 or cislo_volby == 107 or cislo_volby == 100 or cislo_volby == 102 or cislo_volby == 110 or cislo_volby == 207 or cislo_volby == 208 or cislo_volby == 209:
            cislo1 = nacti_cislo(False, "{n}".format(n=first_number))
            cislo2 = is_not
        else:
            cislo1 = nacti_cislo(False, "{n}".format(n=first_number))
            cislo2 = nacti_cislo(False, "{n}".format(n=second_number))

        vys_h = vysledek_hlaska(cislo_volby)
        vysledek = mathfunc(cislo_volby, cislo1, cislo2)
        print(vysledek)
        printlog(vys_h, vysledek, cislo1, cislo2)
        pokracovat = dalsi_priklad()


main()
