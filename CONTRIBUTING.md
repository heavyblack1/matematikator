# Contributing Guide

Cli version put in branch Matematikator-CLI
Gui version put in branch Matematikator-GUI

## Download plugin for your editor

This will setup your editor properly or setup editor [manually](#Manual-Setup) 

<https://editorconfig.org/#download>

## Variable name:

> my_variable = 1

## Use autopep8

enable in VsCode setting>TextEditor>Formating>Formating on save = on

> pip install autopep8

## Manual Setup

-   Tab space is 4 spaces
-   Encoding:utf-8
-   Line end lf

# Translate

**.settings.yaml** add your language is section **language**: like **cz:**
copy paste all vars translate all in **"word to translate"** like:

```yaml
  cz:
    author: "Autor:"
    licence_agree: "Souhlasíte s licencí: y/n"
    first_number: "Zadej první číslo:"
    second:number: "Zadej druhé číslo:"
```
