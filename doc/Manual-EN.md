# Installation

Download and install python 3
<https://www.python.org/downloads/>

Insert and run in the terminal (cmd.exe) to install libraries

> `pip install -r requirements.txt`

# Launch

just open it and if you have installed python with add to path it will run automatically in python
