# Instalace

Stahnout a naistalovat python 3
<https://www.python.org/downloads/>

Vložte a spustěte v terminalu(cmd.exe) pro instalaci knihoven

> `pip install -r requirements.txt`

# Spuštění

stačí otevřít a pokud jste nainstalovali python s možností add to path spustíse automaticky v pythonu
